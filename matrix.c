#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <time.h>

int* allocVector(int n) {
  return (int*) malloc(n * sizeof(int));
}

int** allocMatrix(int rows, int cols) {
  int **newM = (int**) malloc(rows * sizeof(int*));

  for (int i = 0; i < rows; i++)
    newM[i] = allocVector(cols);

  return newM;
}

void readMatrix(FILE *f_in, int **M, int rows, int cols) {
  for (int i = 0; i < rows; i++)
    for (int j = 0; j < cols; j++)
      fscanf(f_in, "%d", &(M[i][j]));
}

void writeMatrix(FILE *f_out, int **M, int rows, int cols) {
  for (int i = 0; i < rows; i++) {
    for (int j = 0; j < cols; j++)
      fprintf(f_out, "%d ", M[i][j]);
    fprintf(f_out, "\n");
  }
}

int** transpose(int **M, int rows, int cols) {
  int **MT = allocMatrix(cols, rows);

  for (int i = 0; i < rows; i++)
    for (int j = 0; j < cols; j++)
      MT[j][i] = M[i][j];

  return MT;
}

int dotProd(int *V1, int *V2, int n) {
  int dp = 0;

  for (int i = 0; i < n; i++)
    dp += V1[i] * V2[i];

  return dp;
}

int** matrixProdMaster(int **M1, int **M2, int rows, int cols, int nb_workers) {
  int **M2T = transpose(M2, cols, rows);
  int **M3 = allocMatrix(rows, rows);
  int offset = 0;

  for (int i1 = 0; i1 < rows; i1++)
    MPI_Bcast(M2T[i1], cols, MPI_INT, 0, MPI_COMM_WORLD);

  while (offset < rows) {
    for (int i = 0; i < nb_workers && offset+i < rows; i++)
      MPI_Send(M1[offset+i], cols, MPI_INT, i+1, 0, MPI_COMM_WORLD);

    for (int k = 0; k < nb_workers && offset+k < rows; k++)
      MPI_Recv(M3[offset+k], cols, MPI_INT, k+1, 0, MPI_COMM_WORLD,
	       MPI_STATUS_IGNORE);

    offset += nb_workers;
  }

  return M3;
}

void matrixProdWorker(int rows, int cols, int iterations) {
  int **M2T = allocMatrix(cols, rows);
  int *R_dp = allocVector(cols);
  int *R = allocVector(cols);

  for (int i1 = 0; i1 < rows; i1++)
    MPI_Bcast(M2T[i1], cols, MPI_INT, 0, MPI_COMM_WORLD);

  for (int i2 = 0; i2 < iterations; i2++) {
    MPI_Recv(R, rows, MPI_INT, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

    for (int i3 = 0; i3 < rows; i3++)
      R_dp[i3] = dotProd(R, M2T[i3], cols);

    MPI_Send(R_dp, cols, MPI_INT, 0, 0, MPI_COMM_WORLD);
  }
}

void getInputs(char *path, int ***p_M1, int ***p_M2, int *p_rows, int *p_cols) {
  FILE *f_in = fopen(path, "r");
  
  fscanf(f_in, "%d %d", p_rows, p_cols);

  *p_M1 = allocMatrix(*p_rows, *p_cols);
  *p_M2 = allocMatrix(*p_cols, *p_rows);
  readMatrix(f_in, *p_M1, *p_rows, *p_cols);
  readMatrix(f_in, *p_M2, *p_cols, *p_rows);

  fclose(f_in);
}

int main(int argc, char **argv) {
  int comm_size, comm_rank, q, r, iterations, rows, cols;
  int **M1, **M2, **M3;
  double start, end;

  MPI_Init(&argc, &argv);
  MPI_Comm_size(MPI_COMM_WORLD, &comm_size);
  MPI_Comm_rank(MPI_COMM_WORLD, &comm_rank);

  if (comm_rank == 0)
    getInputs(argv[1], &M1, &M2, &rows, &cols);

  // Sync before timing
  MPI_Barrier(MPI_COMM_WORLD);
  start = MPI_Wtime();

  MPI_Bcast(&rows, 1, MPI_INT, 0, MPI_COMM_WORLD);
  MPI_Bcast(&cols, 1, MPI_INT, 0, MPI_COMM_WORLD);

  if (comm_rank == 0) {
    M3 = matrixProdMaster(M1, M2, rows, cols, comm_size-1);

    end = MPI_Wtime();
    printf("Execution time: %.6f sec\n", end-start);

    writeMatrix(stdout, M3, rows, rows);
  } else {
    q = rows/(comm_size-1);
    r = rows%(comm_size-1);

    iterations = q + (comm_rank <= r ? 1 : 0);

    matrixProdWorker(rows, cols, iterations);
  }

  MPI_Finalize();
  return 0;
}
