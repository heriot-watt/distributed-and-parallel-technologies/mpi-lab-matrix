# Matrix multiply

Experimenting with the matrix multiply, trying to optimise
the communication between the processes.

# Instructions

## Quick run

Just run the following:

    sbatch script
    
You can also specify the matrix size (avail. 2 [default], 5, 2000):

    sbatch script 2000

## Scripts

- `script` is the batch script for sbatch
- `init` loads the test matrices
- `clean` helps cleaning the repo

## Configuration

If you want to change the configuration for `sbatch`,
you can change it `script`:

    #SBATCH --partition=amd-longq
    #SBATCH -t00:05:00
    #SBATCH -n 3
    #SBATCH -J matrix
    #SBATCH --error=sbatch.err
    #SBATCH --output=sbatch.out

The `-n` option is for the number of processes.
You can also ask for more nodes (ex: `-N2 -n 4`).

## Outputs

The output is forwarded to `sbatch.out`.

It contains a sum up of the configuration used and the execution time,
followed by the multiply result.

If you want the whole output:

    cat sbatch.out
    
If you want the performance only:

    head -3 sbatch.out

Errors will be forwarded to `sbatch.err`.

## Additional commands
    
You can use:
- `squeue` for checking the status of all the jobs
- `scontrol show job JOBID` for checking your job status
- `scancel JOBID` to cancel a job

# Test matrices

The matrices used for test are loaded from the `init` script and comes from:

    wget https://www.macs.hw.ac.uk/~rs46/files/matrices/2MAT_2
    wget https://www.macs.hw.ac.uk/~rs46/files/matrices/2MAT_5_80_99
    wget https://www.macs.hw.ac.uk/~rs46/files/matrices/2MAT_2000_10_65536

## Results

The tests have been made with only **1 node and 3 processes**.

This version took around **17s** to multiply square matrices of **size 2000**.